package rest.api.demo;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;

public class RestassuredDemo {

	private static RequestSpecification requestSpec;

    @BeforeClass
    public static void createRequestSpecification() {
    
        requestSpec = new RequestSpecBuilder().
            setBaseUri("http://dev.vistapi.vistarooms.com/api/").
            build();
    }
    
    @Parameters({ "property_id", "checkin", "checkout", "guestNo", "adultNo", "childNo"})
	@Test(priority = 1)
	public void docheck_PriceBreakup(String property_id, String checkin, String checkout, int guestNo, int adultNo, int childNo)
	{
		given().
        spec(requestSpec).
        log().all()
        .param("property_id", property_id)
        .param("checkin", checkin)
        .param("checkout", checkout)
        .param("guest", guestNo)
        .param("adult",adultNo)
        .param("child",childNo).
        when().
        get("price-breakup").
        then().
        assertThat().
        log().all().
		statusCode(200);
        
	}
    @Parameters({ "user_id", "property_id", "checkin", "checkout", "guestNo", "adultNo", "childNo", "coupon_code"})
    @Test(priority = 2, testName = "docheck_BookNow")
    	public void docheck_Booknow(int user_id, int property_id, String checkin, String checkout, int guestNo, int adultNo, int childNo, String coupon_code)
    	{
    		given().
            spec(requestSpec).
            log().all()
            .param("user_id", user_id)
            .param("property_id", property_id)
            .param("checkin", checkin)
            .param("checkout", checkout)
            .param("guest", guestNo)
            .param("adult",adultNo)
            .param("child",childNo)
            .param("coupon_code",coupon_code)
            .when().
            post("book-now")
    	    .then()
    	    .assertThat()
    	    .log().all()
    	    .statusCode(200)
    	    .contentType(ContentType.JSON);
    		
    	}
    	 
    }
